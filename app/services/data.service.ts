import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RespuestaNews } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) {}

  getNews(page) {
    return this.http.get<RespuestaNews>(`https://newsapi.org/v2/top-headlines?language=es&apiKey=0bfa6c3185774273ae3e64f29668bdaa&page=${page}`)
  }
  getNewsCategory(category,page) {
    return this.http.get<RespuestaNews>(`https://newsapi.org/v2/top-headlines?language=es&apiKey=0bfa6c3185774273ae3e64f29668bdaa&category=${category}&page=${page}`)    
  }
}
