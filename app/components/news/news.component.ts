import { Component, Input, OnInit } from '@angular/core';
import { Article } from 'src/app/interfaces/interfaces';
import { DataService } from '../../services/data.service';
import { Plugins } from '@capacitor/core';
import { ActionSheetController } from '@ionic/angular';
import { DataLocalService } from '../../services/data-local.service';
const { Browser } = Plugins;

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
})
export class NewsComponent implements OnInit {

  page: number = 1;

  @Input() news:Article[] = [];

  constructor(private DataService: DataService, public actionSheetController: ActionSheetController, public DataLocalService: DataLocalService) { }

  loadNews(event?) {
    this.DataService.getNews(this.page).subscribe(
      resp => {
        console.log('News', resp);

        this.news.push(...resp.articles);
        if(event) {
          event.target.complete();
        }
      });
  }

  ngOnInit(): void {
    this.loadNews();
  }
  loadData(event) {
    this.page++;
    this.loadNews(event);
  }


  async openBrowser(url) {
    // On iOS, for example, this will open the URL in Safari instead of
    // the SFSafariViewController (in-app browser)
    await Browser.open({ url });
  }
  async lanzarMenu(article: Article) {
    
    const actionSheet = await this.actionSheetController.create({
      header: 'Opciones',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Share',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
        }
      }, {
        text: 'Favorite',
        icon: 'heart',
        handler: () => {
          console.log('Favorite clicked');
          this.DataLocalService.guardarNoticia(article);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          //console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

}
