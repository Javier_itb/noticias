import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsComponent } from './news/news.component';
import { IonicModule } from '@ionic/angular';
import { NewsCategoryComponent } from './news-category/news-category.component';



@NgModule({
  declarations: [
    NewsComponent,
    NewsCategoryComponent,
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    NewsComponent,
    NewsCategoryComponent,
  ]
})
export class ComponentsModule { }
