import { Component } from '@angular/core';
import { Article } from 'src/app/interfaces/interfaces';
import { DataLocalService } from '../../services/data-local.service';
import { Plugins } from '@capacitor/core';
import { ActionSheetController } from '@ionic/angular';
const { Browser } = Plugins;
const { Share } = Plugins;


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {


  constructor(public DataLocalService: DataLocalService, public actionSheetController: ActionSheetController) {
    this.DataLocalService.cargarFavoritos();
  }

  ngOnInit() {}

  async openBrowser(url) {
    // On iOS, for example, this will open the URL in Safari instead of
    // the SFSafariViewController (in-app browser)
    await Browser.open({ url });
  }
  async lanzarMenu(article: Article) {
    
    const actionSheet = await this.actionSheetController.create({
      header: 'Opciones',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Share',
        icon: 'share',
        handler: async () => {
          let shareRet = await Share.share({
            title: 'See cool stuff',
            text: 'Really awesome thing you need to see right meow',
            url: 'http://ionicframework.com/',
            dialogTitle: 'Share with buddies'
          });
        }
      }, {
        text: 'Delete Favorite',
        icon: 'heart',
        handler: () => {
          this.DataLocalService.deleteFavorite(article);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {}
      }]
    });
    await actionSheet.present();
  }
}
