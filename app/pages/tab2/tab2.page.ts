import { Component, Input } from '@angular/core';
import { Article } from 'src/app/interfaces/interfaces';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  categories: String[] = ["business","entertaiment","general","health","science","sports","technology"];

  @Input() news: Article[] = [];

  constructor() {}

  ngOnInit() {}
}
